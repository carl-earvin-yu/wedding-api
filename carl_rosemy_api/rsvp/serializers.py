from django.core.exceptions import PermissionDenied
from rest_framework import serializers

from .models import RsvpGroup, RsvpPerson


class RsvpPersonSerializer(serializers.ModelSerializer):

    class Meta:
        model = RsvpPerson
        fields = ('name', 'id', 'is_attending', 'role')
        extra_kwargs = {
            'name': {'read_only': True},
            'id': {'read_only': True},
            'role': {'read_only': True}
        }


class RsvpGroupSerializer(serializers.ModelSerializer):

    members = RsvpPersonSerializer(many=True)
    lookup_field = 'passcode'
    extra_kwargs = {
        'url': {'lookup_field': 'passcode'},
        'name': {'read_only': True},
        'id': {'read_only': True},
    }

    class Meta:
        model = RsvpGroup
        fields = ('name', 'id', 'members', 'message', 'email')

    def update(self, instance, validated_data):
        instance = super().update(instance, validated_data)
        members = self.context.get('members', None) or []
        for member in members:
            person = RsvpPerson.objects.get(id=member['id'])
            if person.group_id == instance.id:
                person.is_attending = member['is_attending']
            else:
                raise PermissionDenied("Person does not belong to the RSVP Group")
            person.save()
        return instance
