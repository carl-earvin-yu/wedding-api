from django.db import models
import uuid

# Create your models here.
class RsvpGroup(models.Model):
    name = models.CharField(max_length=32)
    passcode = models.CharField(max_length=64, unique=True)
    message = models.CharField(max_length=512, blank=True)
    email = models.EmailField(blank=True)

    def __str__(self):
        return self.name
    

class RsvpPerson(models.Model):
    class Meta:
        ordering = ["id"]

    name = models.CharField(max_length=64)
    is_attending = models.BooleanField(default=False)
    group = models.ForeignKey(RsvpGroup,
        on_delete=models.DO_NOTHING,
        blank=True, null=True,
        related_name='members')
    role = models.CharField(max_length=24,
        default="guest")

    def __str__(self):
        return self.name
