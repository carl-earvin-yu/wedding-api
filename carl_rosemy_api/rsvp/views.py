from django.shortcuts import render
from rest_framework import viewsets, mixins
from rest_framework.response import Response

from .models import RsvpGroup
from .serializers import RsvpGroupSerializer


# Create your views here.
class RsvpGroupViewSet(viewsets.GenericViewSet, mixins.RetrieveModelMixin):
    model = RsvpGroup
    serializer_class = RsvpGroupSerializer
    lookup_field = 'passcode'

    def get_queryset(self):
        return RsvpGroup.objects.all()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        data = request.data
        context = self.get_serializer_context()
        context['members'] = data.pop('members', None) or []
        serializer = self.serializer_class(instance, data=data, partial=partial, context=context)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)
