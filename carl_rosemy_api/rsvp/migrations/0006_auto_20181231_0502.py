# Generated by Django 2.1.4 on 2018-12-31 05:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rsvp', '0005_auto_20181230_0440'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='rsvpperson',
            options={'ordering': ['id']},
        ),
        migrations.RenameField(
            model_name='rsvpgroup',
            old_name='verse',
            new_name='message',
        ),
    ]
