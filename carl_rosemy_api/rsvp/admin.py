from django.contrib import admin

from .models import RsvpPerson, RsvpGroup
# Register your models here.


class RsvpPersonInline(admin.TabularInline):
    model = RsvpPerson


class RsvpPersonAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_attending', 'role', 'group')


class RsvpGroupAdmin(admin.ModelAdmin):
    inlines = [RsvpPersonInline]
    list_display = ('name', 'passcode', 'email', 'message')


admin.site.register(RsvpPerson, RsvpPersonAdmin)
admin.site.register(RsvpGroup, RsvpGroupAdmin)
