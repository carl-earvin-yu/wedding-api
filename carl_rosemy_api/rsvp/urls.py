from django.conf.urls import url, include

from rest_framework import routers

from .views import RsvpGroupViewSet

rsvp_router = routers.DefaultRouter()
rsvp_router.register(r'rsvp_group', RsvpGroupViewSet, base_name='rsvp_group')


urlpatterns = [
    url(r'', include(rsvp_router.urls)),
]
